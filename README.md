# gnu-toolchain

- `make -f cross-linux.mk -j40` riscv64-unknown-linux-gnu
- `make -f cross-linux.mk ARCH=aarch64 -j40` aarch64-unknown-linux-gnu
- `make -f cross-elf.mk -j40` riscv64-unknown-elf
- `make -f cross-elf.mk ARCH=aarch64 -j40` aarch64-unknown-elf

## dependence

- `wget https://gmplib.org/download/gmp-6.2.1/gmp-6.2.1.tar.xz`
- `wget https://ftp.gnu.org/gnu/mpfr/mpfr-4.1.0.tar.gz --no-check-certificate`
- `wget https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz --no-check-certificate`
