PREFIX=$(shell pwd)/output
ifndef DATE
	DATE := $(shell date +%Y_%m_%d_%H_%M_%S)
endif

build-gdb:
	rm -rf build/$@
	mkdir -p build/$@
	cd build/$@ && ../../binutils-gdb/configure \
		--prefix=$(PREFIX) \
		--enable-gdbserver \
		--disable-gas \
		--disable-binutils \
		--disable-ld \
		--disable-gold \
		--disable-gprof
	$(MAKE) -C build/$@ 2>&1 | tee build/$@/build.log
	$(MAKE) -C build/$@ install

pull-repos:
	git submodule update --init --remote

build-linux-rv32:
	$(MAKE) -f cross-linux.mk XLEN=32 DATE=$(DATE) 2>&1 | tee build-$(DATE).log

build-linux-rv64:
	$(MAKE) -f cross-linux.mk DATE=$(DATE) 2>&1 | tee build-$(DATE).log

build-elf-rv32:
	$(MAKE) -f cross-elf.mk XLEN=32 DATE=$(DATE) 2>&1 | tee build-$(DATE).log

build-elf-rv64:
	$(MAKE) -f cross-elf.mk DATE=$(DATE) 2>&1 | tee build-$(DATE).log

build-crossed-linux-rv32:
	$(MAKE) -f cross-linux.mk XLEN=32 DATE=$(DATE) 2>&1 | tee build-$(DATE).log
	$(MAKE) -f crossed-native.mk XLEN=32 DATE=$(DATE) 2>&1 | tee -a build-$(DATE).log

# need riscv-rivai-linux-gnu toolchain
build-rivai-linux-rv32:
	riscv32-rivai-linux-gnu-gcc -v
	$(MAKE) -f crossed-native.mk DATE=$(DATE) \
		XLEN=32 VENDOR=rivai \
		CFLAGS_FOR_HOST="-O3 -mtune=rugrats_conure -fschedule-insns -fschedule-insns2 -static" \
		CXXFLAGS_FOR_HOST="-O3 -mtune=rugrats_conure -fschedule-insns -fschedule-insns2 -static" \
		2>&1 | tee build-$(DATE).log

build-rivai-linux-rv32-vec:
	riscv32-rivai-linux-gnu-gcc -v
	$(MAKE) -f crossed-native.mk DATE=$(DATE) \
		XLEN=32 VENDOR=rivai \
		CFLAGS_FOR_HOST="-O3 -fopt-info-vec -mtune=rugrats_conure -mcpu=rugrats -fschedule-insns -fschedule-insns2 -static" \
		CXXFLAGS_FOR_HOST="-O3 -fopt-info-vec -mtune=rugrats_conure -mcpu=rugrats -fschedule-insns -fschedule-insns2 -static" \
		2>&1 | tee build-$(DATE).log

crossed_dir=/work/home/lding/Projects/gnu-toolchain/build-${DATE}/output-crossed
cc1=$(crossed_dir)/libexec/gcc/riscv32-rivai-linux-gnu/12.0.1/cc1
as=$(crossed_dir)/bin/as
ld=$(crossed_dir)/bin/ld
gcc_dir=/work/home/lding/Projects/rugrats/output/gcc
ifndef FILE
	FILE=/work/home/lding/Projects/temp/hello.c
endif

# make run-crossed CMD="objdump -h bin"
run-crossed:
	@echo "$(CMD)"
	qemu-riscv32 -B 0x100000 -L $(gcc_dir)/sysroot \
		$(crossed_dir)/bin/$(CMD)

compile-and-run:
	# 编译
	qemu-riscv32 -B 0x1000000 -L $(gcc_dir)/sysroot \
		$(cc1) -quiet \
		-iprefix $(gcc_dir)/lib/gcc/riscv32-rivai-linux-gnu/12.0.1/ \
		-isysroot $(gcc_dir)/sysroot \
		-mtune=rocket -mabi=ilp32d -misa-spec=20191213 -march=rv32imafdc_zicsr \
		-o temp.s -O3 $(FILE)
	# 汇编
	qemu-riscv32 -B 0x1000000 -L $(gcc_dir)/sysroot \
		$(as) -o temp.o temp.s
	# 链接
	qemu-riscv32 -B 0x1000000 -L $(gcc_dir)/sysroot \
		$(ld) --sysroot=$(gcc_dir)/sysroot --eh-frame-hdr -melf32lriscv \
		-dynamic-linker /lib/ld-linux-riscv32-ilp32d.so.1 \
		$(gcc_dir)/sysroot/usr/lib/crt1.o \
		$(gcc_dir)/lib/gcc/riscv32-rivai-linux-gnu/10.2.1/crti.o \
		$(gcc_dir)/lib/gcc/riscv32-rivai-linux-gnu/10.2.1/crtbegin.o \
		-L$(gcc_dir)/lib/gcc/riscv32-rivai-linux-gnu/10.2.1 \
		-L$(gcc_dir)/lib/gcc \
		-L$(gcc_dir)/riscv32-rivai-linux-gnu/lib \
		-L$(gcc_dir)/sysroot/lib \
		-L$(gcc_dir)/sysroot/usr/lib \
		temp.o -lgcc --push-state --as-needed -lgcc_s --pop-state -lc -lgcc --push-state --as-needed -lgcc_s --pop-state \
		$(gcc_dir)/lib/gcc/riscv32-rivai-linux-gnu/10.2.1/crtend.o \
		$(gcc_dir)/lib/gcc/riscv32-rivai-linux-gnu/10.2.1/crtn.o
	# 运行
	qemu-riscv32 -B 0x1000000 -L $(gcc_dir)/sysroot a.out

clean:
	rm -rf build-2*
